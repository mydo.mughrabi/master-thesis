import pandas as pd


class RegexCreator:
    @classmethod
    def from_dataset(cls, dataset_path):
        data = pd.read_csv(dataset_path)
        column_result = [r'|'.join(data[col].unique()) for col in data]
        result = r'|'.join(column_result)
        result = result.replace('(', '\\(')
        result = result.replace(')', '\\)')
        result = result.replace('/', '\\/')
        print(result)
        print(result.count('|'))
        return result

    @classmethod
    def from_file(cls, file_path):
        with open(file_path, 'r') as file:
            data = file.read()
        return data
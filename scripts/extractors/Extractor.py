import json
import re
from abc import ABC, abstractmethod
from pathlib import Path
import spacy
from spacy import displacy
from scripts.Constants import target_words_regex

# todo: refactoring ideas
#   2. no need to pass group_name as it is a class variable now
#   3. you might want to keep the (value, location) as a dict entries {value: '', location: ''} instead of pair
from scripts.extractors.RegexCreator import RegexCreator


class Extractor(ABC):

    def __init__(self, regex, condition=None):
        self.regex = regex
        self.condition = condition

    def exctract_all_regex_values_from_text_with_condition(self, text, group_name=None):
        results = []
        search_result = re.search(self.regex, text, re.IGNORECASE)

        # get a match, throw it, rerun the regex till there is no matches left.
        while search_result:
            result = search_result.group(group_name)
            cut_begin, cut_end = search_result.span(group_name)
            if not self.condition or self.condition(result):
                results.append({'value': result, 'location': (cut_begin + cut_end) // 2})
            text = text[:cut_begin] + ' ' + text[cut_end:]
            search_result = re.search(self.regex, text, re.IGNORECASE)

        return results, text

    @abstractmethod
    def extract_from_text(self, text):
        pass


class R0Extractor(Extractor):
    # todo: (?P<digits_group>[ ,(]\d+\.?\d?+) might be a better digit extractor as it is a single one
    def __init__(self):
        digits_extractors = ['\d\.\d+',
                             '[ (][^\s]*[1-9].*? ']  # digit.digit | space or braket, followed by non-space characters, contains a number, ends with the next space.
        digits_extractor_regex = r'|'.join(digits_extractors)
        self.group_name = 'digits_group'
        self.extraction_regex = f"({target_words_regex})[^.]*(?P<{self.group_name}>{digits_extractor_regex}).*?\."
        super().__init__(self.extraction_regex, self.is_acceptable_digits_result)

    # primitive filtering for unaccepted digit result
    # for now, reject only the word COVID-19 as it is the most common problem in the dataset
    def is_acceptable_digits_result(self, digits_result):
        digits_result = digits_result.strip()
        disease_explaination_regex = 'covid-19|sars-cov-2|h1n1'
        contains_disease_explaination = re.search(disease_explaination_regex, digits_result, re.IGNORECASE)
        return not contains_disease_explaination

    def extract_from_text(self, text):
        return self.exctract_all_regex_values_from_text_with_condition(text,
                                                                       group_name=self.group_name)


class YearsExtractor(Extractor):
    def __init__(self):
        self.group_name = 'years_group'
        self.years_extractor_regex = f'(?P<{self.group_name}>\d\d\d\d)'
        super().__init__(self.years_extractor_regex, condition=None)

    def extract_from_text(self, text):
        return self.exctract_all_regex_values_from_text_with_condition(text,
                                                                       group_name=self.group_name)


class CountryExtractor(Extractor):
    def __init__(self):
        self.group_name = 'countries_group'
        countries_regex_path = Path('regex/countries_regex.txt')
        countries_regex = RegexCreator.from_file(countries_regex_path)
        self.countries_extraction_regex = f' (?P<{self.group_name}>{countries_regex}) '
        super().__init__(self.countries_extraction_regex, condition=None)

    def extract_from_text(self, text):
        return self.exctract_all_regex_values_from_text_with_condition(text, group_name=self.group_name)


class MethodExtractor(Extractor):
    def __init__(self):
        self.group_name = 'method_group'
        method_regex_path = Path('regex/methods_regex.txt')
        methods_regex = RegexCreator.from_file(method_regex_path)
        self.method_extraction_regex = f' (?P<{self.group_name}>{methods_regex}) '
        super().__init__(self.method_extraction_regex, condition=None)

    def extract_from_text(self, text):
        return self.exctract_all_regex_values_from_text_with_condition(text, group_name=self.group_name)


class ConfidenceExtractor(Extractor):
    def __init__(self):
        self.group_name = 'confidence_interval_group'
        self.method_extraction_regex = f"(?P<{self.group_name}>\d+(?:.(?!\d))) (confidence interval|c\.i|ci) "
        super().__init__(self.method_extraction_regex, condition=None)

    def extract_from_text(self, text):
        return self.exctract_all_regex_values_from_text_with_condition(text, group_name=self.group_name)


class DiseaseExtractor(Extractor):
    def __init__(self):
        self.group_name = 'disease_group'
        disease_regex_path = Path('regex/disease_regex.txt')
        disease_regex = RegexCreator.from_file(disease_regex_path)
        self.disease_extraction_regex = f' (?P<{self.group_name}>{disease_regex}) '
        super().__init__(self.disease_extraction_regex, condition=None)

    def extract_from_text(self, text):
        return self.exctract_all_regex_values_from_text_with_condition(text, group_name=self.group_name)


nlp = spacy.load('en_core_web_sm')


class SpacyLocationExtractor(Extractor):

    def __init__(self):
        self.group_name = 'countries_group'

    def extract_from_text(self, text):
        # options are 'xx_ent_wiki_sm' for wikipedia schema, and 'en_core_web_sm' for spacy schema
        doc = nlp(text)
        results = []
        for ent in doc.ents:
            if ent.label_ in ['GPE', 'LOC']:
                results.append({'value': ent.text, 'location': (ent.start + ent.end) // 2})
                text = text[:ent.start] + ' ' + text[ent.end:]
        # displacy.render(doc, style="ent", jupyter=True)
        return results, text


class ExtractorPipeline:
    def __init__(self):
        self.extractors = [YearsExtractor(), MethodExtractor(), SpacyLocationExtractor(), DiseaseExtractor(),
                           ConfidenceExtractor(), R0Extractor()]

    def extract_from_text(self, text):
        result = {}
        for extractor in self.extractors:
            extractor_result, text = extractor.extract_from_text(text)
            if extractor_result:
                result[extractor.group_name] = extractor_result
        return json.dumps(result)


if __name__ == "__main__":
    import pandas as pd

    data_with_target_words_in_abstract = pd.read_csv('data/first_filter_data.csv')
    r0_extractor = R0Extractor()
    data_with_target_words_in_abstract['r_values'] = data_with_target_words_in_abstract['abstract'].apply(
        r0_extractor.extract_from_text)
    # filter the rows with empty r_values lists
    data_with_target_words_in_abstract = data_with_target_words_in_abstract[
        data_with_target_words_in_abstract['r_values'].astype(bool)]

    data_with_target_words_in_abstract = data_with_target_words_in_abstract.drop(labels=['r_values'], axis=1)
    pipeline = ExtractorPipeline()
    print(data_with_target_words_in_abstract.shape)
    data_with_target_words_in_abstract['extracted'] = data_with_target_words_in_abstract['abstract'].apply(
        pipeline.extract_from_text)
    data_with_target_words_in_abstract.to_csv('temp.csv')

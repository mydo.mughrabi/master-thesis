import itertools

import networkx as nx
import json
import numpy as np

class GraphCreator():
    def ConnectGraphNodes(self, node_groups):
        result_graph = nx.Graph()

        graph_nodes = []
        graph_distances = []

        for group in node_groups:
                for node in group.values():
                    for node_value in node:
                        print(node_value)
                        graph_nodes.append(node_value[0])
                        graph_distances.append(node_value[1])

        result_graph.add_nodes_from(graph_nodes)

        #add edges to create a fully connected graph with their weights
        edges_values = itertools.combinations(graph_nodes,2)
        edges_weights = [abs(i - j) for (i,j) in itertools.combinations(graph_distances,2)]
        for i, edge in enumerate(edges_values):
            result_graph.add_edge(edge[0], edge[1], weight=edges_weights[i])

        min_span_tree = nx.minimum_spanning_tree(result_graph)
        return min_span_tree


import pandas as pd
import pylab
data_with_target_words_in_abstract = pd.read_csv('data/temp.csv')
print(data_with_target_words_in_abstract['extracted'].values[2])
nodes = json.loads(data_with_target_words_in_abstract['extracted'].values[2])
nx.draw(GraphCreator().ConnectGraphNodes(nodes), with_labels=True)
pylab.show()